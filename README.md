# Treasure-Thief
## About

A 2-player local multiplayer game about robbing a treasure game, but a warden is watching over the cave.

## Download & Use
To open and play it, you must have [Greenfoot](https://www.greenfoot.org/download) installed. <br>
The newest release of the game can be downloaded [here](https://codeberg.org/Learning-Java/Treasure-Thief/releases).
