import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The world in which the two players can move in.
 * 
 * @MagicLike
 * @v0.1
 */
public class TreasureCave extends World
{
    private int wallLimit = 50; // Variable for the number of walls
    private int coinLimit = 10; // Variable for the number of coins
    private int coinCount; // Counting how many coins have been collected
    public TreasureCave()
    {    
        // Create a new world with 140x80 cells with a cell size of 10x10 pixels.
        super(140, 80, 10); 
        
        // Spawn Tux
        Tux tux1 = new Tux ();
        addObject (tux1, 70, 5);
        
        // Spawn Player
        Player player1 = new Player ();
        addObject (player1, 70, 75);
        
        // Show how to play the game
        showText("Player: Control your character with WASD and collect all coins to win!",106,75);
        showText("Tux: Control Tux with the arrows and catch the player to win!",102,77);
        
        spawnCoins();
        spawnWalls();
        coinCounter();
    }
    
    public void spawnWalls()
    {
        // Walls - spawning walls at random locations
        for(int i=0; i<wallLimit; i++){
        int x = Greenfoot.getRandomNumber(getWidth());  // Generate random x-coordinate
        int y = Greenfoot.getRandomNumber(getHeight()); // Generate random y-coordinate
        
        if (getObjectsAt(x,y,Actor.class).isEmpty()){   // Makes walls not spawning on top of other Actors
        Wall wall = new Wall ();
        addObject (wall, x, y);
        }
        }
    }
    
    public void spawnCoins()
    {
        // Coins - spawning coins at random locations
        for(int i=0; i<coinLimit; i++){
        int x = Greenfoot.getRandomNumber(getWidth());  // Generate random x-coordinate
        int y = Greenfoot.getRandomNumber(getHeight()); // Generate random y-coordinate
        
        if (getObjectsAt(x,y,Actor.class).isEmpty()){   // Makes walls not spawning on top of other Actors
        Coin coin = new Coin ();
        addObject (coin, x, y);
        }
        }
    }
    
    public void endScreenTux() // Showing, when tux has won the game
    {
        showText("Tux won!",70,40);   // Print out "Tux won!"
        Greenfoot.stop();   // Stop the game -> The players must restart the game to play again
    }
    
        public void setCount()  // Add +1 to the snack counter
    {
        coinCount++;
    }
    
        public void coinCounter()  // Show the snack counter
    {
        showText("Collected coins:"+coinCount,9,3); //  Maybe add a coinCoint / (of) coinLimit
    }
    
    public void endScreenPlayer() // Showing, when the player has won the game
    {
        if(coinCount==coinLimit)
        {
            showText("Player won!",70,40);   // Print out "You won!"
            showText("Score:"+coinCount,70,42);  // Print out the score, the player reached
            Greenfoot.stop();   // Stop the game -> The player must restart the game to play again
        }
    }
}
