import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Controller for the two characters.
 * 
 * @MagicLike
 * @v0.1
 */
public class Character extends Actor
{
    public void act()
    {
        
    }
    
    public void movement(char direction)
    {
        int mS = 1; // Speed variable [cells]
        
        int x = getX(); // Get x-coordinates
        int y = getY(); // Get y-coordinates
        
        if (direction == 'r' && getObjectsAtOffset(1,0,Wall.class).isEmpty()) setLocation(x+mS, y);
        else if (direction == 'l' && getObjectsAtOffset(-1,0,Wall.class).isEmpty()) setLocation(x-mS, y);
        else if (direction == 'u' && getObjectsAtOffset(0,-1,Wall.class).isEmpty()) setLocation(x, y-mS);
        else if (direction == 'd' && getObjectsAtOffset(0,1,Wall.class).isEmpty()) setLocation(x, y+mS);
    }
}
