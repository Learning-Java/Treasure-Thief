PROJECT TITLE: Treasure Thief
PURPOSE OF PROJECT: Learning Java
VERSION or DATE: v0.1
HOW TO START THIS PROJECT: Dowload Greenfoot & open the .gfar release
AUTHORS: MagicLike
USER INSTRUCTIONS: Use WASD for the Player and the arrow keys for Tux. When all coins get collected, the Player wins. When Tux catches the player, Tux wins.
