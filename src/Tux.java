import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The warden Tux.
 * 
 * @MagicLike
 * @v0.1
 */
public class Tux extends Character
{
    public void act()
    {
        keys();
        catch_player();
    }
    
    public void keys() //   Controls to move Tux
    {
        if (Greenfoot.isKeyDown("up"))
        {
            movement('u');
        }
        else if (Greenfoot.isKeyDown("left"))
        {
            movement('l');
        }
        else if (Greenfoot.isKeyDown("down"))
        {
            movement('d');
        }
        else if (Greenfoot.isKeyDown("right"))
        {
            movement('r');
        }
    }
    
    public void catch_player() //   Trigger Tux' end screen
    {
        if(isTouching(Player.class))
        {
            ((TreasureCave)getWorld()).endScreenTux();
        }
    }
}
