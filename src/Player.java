import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The player who robbs the cave.
 * 
 * @MagicLike
 * @v0.1
 */
public class Player extends Character
{
    public void act()
    {
        keys();
        collect();
        update();
    }
    
    public void keys() //   Controls to move Player
    {
        if (Greenfoot.isKeyDown("w"))
        {
            movement('u');
        }
        else if (Greenfoot.isKeyDown("a"))
        {
            movement('l');
        }
        else if (Greenfoot.isKeyDown("s"))
        {
            movement('d');
        }
        else if (Greenfoot.isKeyDown("d"))
        {
            movement('r');
        }
    }
    
    public void collect() // Collecting coins...
    {
        if(isTouching(Coin.class))
        {
            removeTouching(Coin.class);
            ((TreasureCave)getWorld()).setCount();
        }
    }
    
    public void update()    // Updates elements in the world important for Player
    {
        ((TreasureCave)getWorld()).coinCounter();
        ((TreasureCave)getWorld()).endScreenPlayer();
    }
}
